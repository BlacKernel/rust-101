use std::env;

mod api;
use api::roll::{roll, RollType};

fn main() {
    let args: Vec<String> = env::args().collect();

    let num_dice: usize = args[1].parse::<usize>().unwrap();
    let roll_type_input: char = args[2].parse::<char>().unwrap();
    let num_sides: isize = args[3].parse::<isize>().unwrap();

    let roll_type = match roll_type_input {
        'd' => RollType::Dice,
        'f' => RollType::Fudge,
        _   => panic!("rust-roller: Only dice and fudge roll types are currently implemented")
    };

    let rolls = roll(num_dice, roll_type, num_sides).unwrap();

    println!("Dice: {:?}", rolls);
    
    println!("Total: {}", rolls.iter().fold(0, |mut sum, &x| {sum += x; sum}));
}

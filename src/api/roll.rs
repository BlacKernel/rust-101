use rand::{thread_rng, Rng};

#[derive(PartialEq)]
pub enum RollType {
    Dice,
    Fudge,
}

pub fn roll(num_dice: usize, roll_type: RollType, num_sides: isize) -> Option<Vec<isize>> {
    let mut rng = thread_rng();
    let mut rolls: Vec<isize> = Vec::new();

    if roll_type == RollType::Dice {
        for _roll in 1..=num_dice {
            let cur_die: isize;
            if num_sides < 0        { cur_die = rng.gen_range(num_sides..=-1); }
            else if num_sides > 0   { cur_die = rng.gen_range(1..=num_sides); }
            else { cur_die = 0; }
            rolls.push(cur_die);
        }
    } else if roll_type == RollType::Fudge {
        if (num_sides % 3) != 0 { eprintln!("rust-roller: Please use an multiple of 3 sides for fudge dice"); return None; }
        if num_sides < 0 { eprintln!("rust-roller: Negitive fudge dice are equivilant to positive fudge dice. Please roll positive fudge dice instead."); return None; }

        for _roll in 1..=num_dice {
            let cur_die: isize;
            match rng.gen_range(1..num_sides) % 3 {
                0 => cur_die = 1,
                1 => cur_die = 0,
                2 => cur_die = -1,
                _ => panic!("YOU BROKE MATH?!"),
            }
            rolls.push(cur_die);
        }
    }

    return Some(rolls);
}


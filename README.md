# Rust-Roller Dice Roller Tutorial Application

This is a simple dice rolling application that follows along with my [Hacker Public Radio](https://hackerpublicradio.org) course on learning the [rust programming language](https://rust-lang.org).

I will attempt to make the commits follow the episodes pretty closely with one commit after every episode with the episoede number in the commit message.

### Usage:
`rust-101 <number-of-dice> <number-of-sides-per-die>`

### Output:
`Total: <sum-of-all-dice-values>`

----

## List of Episodes
[Rust 101: Episode 0 - What in Tarnishing?](https://hackerpublicradio.org/eps.php?=id3426)

[Rust 101: Episode 1 - Hello, World!](https://hackerpublicradio.org/eps.php?id=3453)

[Rust 101: Episode 2 - Rolling With the Errors](https://hackerpublicradio.org/eps.php?=id3519)

[Rust 101: Episode 3 - Functionally Insane](https://hackerpublicradio.org/eps.php?id=3681)

----

Contact Info:

[at blackernel at nixnet dot social](https://nixnet.social/BlacKernel)

[izzyleibowitz at pm dot me](mailto@izzyleibowitz@pm.me)
